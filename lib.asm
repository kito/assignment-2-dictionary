section .text

%define SYS_EXIT 60
%define SYS_OUT 1
%define SYS_WRITE 1

global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global read_string
global parse_uint
global parse_int
global string_copy

; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, SYS_EXIT ;запись кода системного вызова
    syscall ;system call
    ret

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax,rax ;i=0
    .loop: ;while(str[i]!=0) i++
        cmp byte[rax+rdi],0 ;если конец -> переход на .end
        je .end
        inc rax ;i++
        jmp .loop
    .end:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в поток вывода
;rdi - address, rsi - дескриптор
print_string:
    xor rax, rax
    push rdi
    push rsi
    call string_length
    pop rsi
    pop rdi
    mov rdx, rax ;rdx = len(str)
    push rsi
    mov rsi, rdi ;rsi = адрес строки
    pop rdi;дескриптор stdout
    mov rax, 1 ;установка номера системного вызова
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char: 
    push rdi ;кладем код символа в память, потому что системный вызов берет адрес символа в rsi
    mov rdx, 1     
    mov rsi, rsp ;получаем адрес символа через указатель на стек
    pop rdi
    mov rax, SYS_WRITE      ; Код системного вызова "Вывод"
    mov rdi, SYS_OUT      ; Дескриптор stdout
    syscall         ; Системный вызов
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    xor rax, rax
    mov rdi, '\n' ;загружаем символ перевода строки и вызываем print_char
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    xor rax, rax 
    mov r8, 10 ;основание системы
    mov r9, rsp ; сохраняем указатель на стек
    mov rax, rdi ;rax = N
    ;будем идти с конца и записывать в стек символы последовательно. а потом тупа вызовем print_string
    push 0 ;последний символ '0', конец строки
    .loop:
        xor rdx,rdx ;data register
        div r8 ;целая часть в rax, остаток в rdx
        add rdx, '0' ;переводим символ в ASCII
        dec rsp ;идем в обратную сторону от конца стека. push и pop делают тоже самое НО шаг != 1
        mov byte[rsp], dl ;записываем символ из rdx по адресу rsp
        cmp rax,0 ;если конец выходим 
        je .end
        jmp .loop ;иначе show must go on

    .end:
        mov rdi, rsp ;записываем адрес первого символа и вызываем print_string
        push r9
        call print_string
        pop r9
        mov rsp, r9 ; восстанавливаем stack ptr
        ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    xor rax, rax
    cmp rdi,0
    jl .neg ;if n<0 переход на .negative
    jmp print_uint ;если >=0 то просто выводим как беззнаковое

    .neg
        push rdi  ;сохраняем число
        mov rdi, '-' ;выводим -
        call print_char
        pop rdi
        neg rdi ;берем отрицательное число
        jmp print_uint ;выводим "модуль" числа

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rax, rax
    xor rcx, rcx ;i
    xor r8, r8 ;очищаем все нужные регистры
    xor r9, r9
    .loop: ;r8b <- запись 8 bit char
        mov r8b, byte[rdi+rcx] ;r8 = s1[i]
        mov r9b, byte[rsi+rcx] ;r9 = s2[i]
        cmp r8b,r9b 
        jne .no ;if s1[i]!=s2[i] return  false
        cmp r8b,0
        je .yes ;if s1[i]==s2[i]=='0' return true; то есть если все символы совпали
        inc rcx ;i++
        jmp .loop
    
    .yes:
        mov rax, 1   ;return true
        ret
    .no:
        mov rax,0 ;return false
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    xor rax, rax ;rax=0 -> input. stdin дескриптор
    xor rdi,rdi 
    mov rdx, 1 ;len = 1
    push 0 ;кладем пустой символ на стек, теперь он по адресу [rsp]
    mov rsi, rsp ;загружаем адрес символа
    syscall
    pop rax ;загружаем сивмол со стека в rax
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    xor rcx, rcx
    xor rax, rax
    ;rdi - адрес начала rsi - размер  буфера
    .loop:
        push rcx
        push rsi
        push rdi
        call read_char ;читаем символ
        pop rdi
        pop rsi
        pop rcx

        cmp rax, 0 ;если конец, переход на .end
        je .end

        cmp rax, ' ' ;проверки на пустые символы, переход на .skip_space
        je .skip_whitespace
        cmp rax, '	'
        je .skip_whitespace
        cmp rax, 0xA
        je .skip_whitespace

        mov [rdi+rcx], rax  ;загруженный символ кладем по адресу. word[i]=char
        inc rcx ;i++
        cmp rcx, rsi ;if(i>buffer_length) overflow error
        jge .error

        jmp .loop

    .skip_whitespace:
        cmp rcx,0 ;если слово еще не началась, то пропуск.
        je .loop
        jmp .end ;а если уже были символы, значит этот пробел-конец слова. уходим. уходим. уходим.
    .error:                                                                ;наступят времена почище.
        xor rax, rax ;возврат нулей                                         ...ВЛАДИВОСТОК 2000
        xor rdx, rdx
        ret
    .end:
        xor rax, rax
        mov [rdi+rcx], rax ;last null. s[i+1] = '0'
        mov rax, rdi ;addr
        mov rdx, rcx ;len
        ret
    




; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rcx, rcx
    mov r8, 10 ;основание системы
    .loop: ;movzx - extend with 0
        movzx r9, byte[rdi+rcx] ;r9 = s[i]
        cmp r9,0 ;if r9==0 end
        je .end
        cmp r9b, '0' ;проверка на то что цифра. то есть код меньше 9 и больше 0.
        jl .end     ;если не цифра, дальше не парсим.
        cmp r9b, '9'
        jg .end

        mul r8 
        sub r9b, '0' ;получаем цифру,  вычитая из кода символа код 0. (ascii)
        add rax, r9 ;rax = rax*10 + digit. то есть сдвигаем на рязряд и добавляем цифру
        inc rcx ;i++
        jmp .loop

    .end
        mov rdx, rcx ;записываем в rdx длину. rdx = i
        ret


; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    xor rdx, rdx ;чистим регистры
    mov rcx, rdi 
    cmp byte[rcx], '-';if s[i]== "-" переход на .neg
    je .neg
    jmp .pos ;иначе на .pos
    .neg:
        inc rcx ;i+=1, чтобы убрать минус
        mov rdi, rcx
        push rcx
        call parse_uint ;читаем беззнаковое, "модуль"
        pop rcx
        neg rax ;берем отрицательное
        inc rdx ;rdx = true, все ок
        ret
    .pos:
        mov rdi, rcx
        jmp parse_uint ;просто парсим положительное и не паримся
        

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0

string_copy: ;args: rdi, rsi, rdx
    xor rax, rax
    xor rcx, rcx ;i=0
    push rsi
    push rdi
    push rcx
    push rdx
    call string_length
    pop rdx
    pop rcx
    pop rdi
    pop rsi

    mov r8, rax ;r8=len(str)
    cmp rdx, r8 ;if len(buffer)<len(string) 
    jl .error
    .loop:
        cmp rcx, r8;if(i> len(str)) break;
        jg .end
        mov r10,[rdi+rcx] ;buffer[i]=string[i]
        mov [rsi+rcx], r10
        inc rcx; i++
        jmp .loop
    
    .error:
        mov rax, 0 ;возвращаем 0
        ret
    .end:
        mov rax, r8 ;возврат длины
        ret



