%define BUFFER_SIZE 256
%define STDOUT 1
%define STDERR 2
%define OFFSET 8

section .rodata
%include "word.inc"
start_msg:
    db 'Enter the key: ', 0
err_msg: 
    db 'Couldn not read the word', 10, 0
not_found_msg: 
    db 'The key was not found in the dictionary', 10, 0
found_msg: 
    db 'Value by key: ', 0

section .text
extern find_word
extern print_dict
%include "lib.inc"

global _start
_start:

    mov rdi, start_msg
    mov rsi, STDOUT     ;стартовое сообщение
    call print_string

    sub rsp, BUFFER_SIZE ;выделяем в стеке память
    mov rsi, BUFFER_SIZE ;задаем максимальную длину ввода для ф-ции read_word
    mov rdi, rsp
    call read_word
    cmp rax, 0 ;что то пошло не так
    jz .err_read ;ошибка ввода
    mov rsi, first ;rsi указывает на первую пару кдюч-значение
    mov rdi, rax ;в rdi- введенное слово
    call find_word 
    cmp rax, 0  ;попытались найти, если нет то переход на .not_found

    je .not_found

    .found:
        add rax, OFFSET ;смещение к адресу начала ключа
        push rax
        mov rsi, STDOUT
        mov rdi, found_msg
        call print_string ;выводим сообщение о том что найдена строка, дальше нужно саму строку вывести

        ;пара состоит из key_string \0 value_string \0
        ;нам нужна value_string
        pop rax ;в rax - адрес ключа
        mov rdi, rax
        call string_length
        inc rax
        add rdi, rax ;rdi = addr + key_len + 1(нуль терминатор)
        mov rsi, STDOUT
        call print_string ;выводим value_string
        jmp .end

    .err_read:
        mov rdi, err_msg
        mov rsi, STDERR ;выводим сообщение об ошибке ввода
        call print_string
        jmp .end

    .not_found:
        mov rdi, not_found_msg
        mov rsi, STDERR ;выводим сообщение о том что слово не найдено
        call print_string
        ;jmp .end

    .end:
        add rsp, BUFFER_SIZE ;восстанавливаем указатель стека и уходим в закат
        call exit


	
