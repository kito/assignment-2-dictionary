section .text

extern string_equals
extern string_length
global find_word

find_word:
        ;rdi string pointer
        ;rsi dictionary pointer
	; Проверяем, что указатель на словарь не равен 0
	test rsi, rsi
	je .not_found

.loop:
        push rsi
        push rdi
        add rsi, 8

        call string_equals

        pop rdi
        pop rsi
	; Если строки равны, переходим к метке .found
        cmp rax, 1
        je .found
	; Если текущий элемент словаря равен 0, переходим к метке .not_found
        mov rsi, [rsi]
        cmp rsi, 0
        je .not_found
        ; Возвращаемся к началу цикла и продолжаем поиск
        jmp .loop

.found:
	; Сохраняем адрес начала вхождения в словарь в регистре rax
        mov rax, rsi
        ; Возвращаем адрес начала вхождения
        ret

.not_found:
        xor rax, rax
        ret

